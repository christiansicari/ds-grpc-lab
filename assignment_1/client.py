import grpc
import assignment1_pb2_grpc, assignment1_pb2


def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = assignment1_pb2_grpc.costStub(channel)
        response = stub.getCost(assignment1_pb2.nameCost(name='Christian', cost=10))


    print("client received: " + response.message)


if __name__ == '__main__':
    run()
