from concurrent import futures
import time
import grpc
import assignment1_pb2_grpc
import assignment1_pb2
_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class cost(assignment1_pb2_grpc.costServicer):

    def getCost(self, request, context):
        msg = "Hello {}, your cost is: {}".format(request.name, len(request.name)*request.cost)
        return assignment1_pb2.costReport(message=msg)


def serve():
    print("Server is online")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    assignment1_pb2_grpc.add_costServicer_to_server(cost(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
