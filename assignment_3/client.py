import grpc
import proto_pb2_grpc, proto_pb2
from time import sleep
def r1(op, v1, v2):
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = proto_pb2_grpc.CalculatorStub(channel)
        print("Calling doCalc with: operation={}, value1={}, value2={}".format(op,v1, v2) )
        response = stub.doCalc(proto_pb2.calcMsg(operation=op, value1=v1, value2=v2))

        print("client received: " + str(response.text))


def r2(text):
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = proto_pb2_grpc.CalculatorStub(channel)
        text="Christian"
        print("Calling strToNumber with: text: ", text)
        response = stub.strToNumber(proto_pb2.Message(text=text))

        print("client received: " + str(response.text))


def name_iterator(names):
    for name in names:
        yield proto_pb2.Message(text=name)

def r3(names):
    print("Calling countStream with: names: ", names)
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = proto_pb2_grpc.CalculatorStub(channel)
        it = name_iterator(names)
        response = stub.countStream(it)
        print("client received: " + str(response.text))



if __name__ == '__main__':
    r1("*", 10, 2)
    r1("xxx", 10, 2)
    r2("Christian")
    r3(["Christian", "Elisa","Katia", "Giovanni"])
