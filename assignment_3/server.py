from concurrent import futures
import time
import grpc
import proto_pb2_grpc
import proto_pb2
_ONE_DAY_IN_SECONDS = 60 * 60 * 24
op = {'+': lambda x, y: x + y,
      '-': lambda x, y: x - y,
      "*": lambda x, y: x * y,
      "/": lambda x, y: x / y}

class Calculator(proto_pb2_grpc.CalculatorServicer):
    def doCalc(self, request, context):
        if request.operation in op.keys():
            res = op[request.operation](float(request.value1), float(request.value2))
            msg = "Response : {}".format(res)
        else:
            msg = "Invalid operator"

        return proto_pb2.Message(text=msg)


    def strToNumber(self, request, context):
        bin = [str(v) for v in list(str.encode(request.text)) ] # return asci code
        msg = "Numerical representation of {} is: {}".format(request.text, ",".join(bin))
        return proto_pb2.Message(text=msg)

    def countStream(self, requests, context):
        lens = 0
        letters = 0
        for r in requests:
            lens += 1
            letters += len(r.text)
        msg = "Total Names: {}, Total Letters: {} ".format(lens, letters)
        return proto_pb2.Message(text=msg)


def serve():
    print("Server is online")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    proto_pb2_grpc.add_CalculatorServicer_to_server(Calculator(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
